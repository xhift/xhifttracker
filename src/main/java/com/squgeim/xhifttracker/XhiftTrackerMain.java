package com.squgeim.xhifttracker;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author squgeim
 */
public class XhiftTrackerMain {

    public static void main(String argv[]) {
        try {
            XhiftHTTPTrackerServer server = new XhiftHTTPTrackerServer();
            System.out.println("\nRunning! Point your browser to http://localhost:6969/ \n");
            
            String ch;
            do {
                Scanner keyboard = new Scanner(System.in);
                System.out.println("q to quit: ");
                ch = keyboard.next();
            } while (! "q".equals(ch));
            
            server.quit();
        } catch (IOException ex) {
            Logger.getLogger(XhiftTrackerMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
