package com.squgeim.xhifttracker;

import com.squgeim.xhiftbencoder.BencodeDataType;
import com.squgeim.xhiftbencoder.BencodeDictionary;
import com.squgeim.xhiftbencoder.BencodeList;
import com.squgeim.xhiftbencoder.BencodeString;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author squgeim
 */
public class XhiftTracker {

    private final HashMap<BencodeString, PeerList> allPeers = new HashMap<>();

    private final int THREAD_SLEEP = 10000; // Refresh every 10 seconds
    private final int ANNOUNCE_INTERVAL = 5; // Announce every 5 seconds
    private final int TIME_TO_LIVE = 10000; // Peer is removed if silent for more than 10 seconds
    private Timer timer = new Timer("PeerListRefresh", true);

    private static XhiftTracker instance = new XhiftTracker();

    private XhiftTracker() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
//                for (BencodeString key : allPeers.keySet()) {
                for (Iterator<Map.Entry<BencodeString, PeerList>> it = allPeers.entrySet().iterator(); it.hasNext(); ) {
                    Map.Entry<BencodeString, PeerList> entry = it.next();
                    //allPeers.get(key).refresh();
                    entry.getValue().refresh();
                    if (entry.getValue().isEmpty()) {
                        it.remove();
                        System.out.println("Empty");
                    } else {
                        System.out.println("Sha: " + entry.getKey());
                        System.out.println("Peers: " + entry.getValue());
                    }
                }
                if(allPeers.isEmpty()) {
                    System.out.println("Empty");
                }
            }
        }, TIME_TO_LIVE, TIME_TO_LIVE);
    }

//    public static void init() {
//        if (instance == null) {
//            instance = new XhiftTracker();
//        }
//    }

    public static void kill() {
        instance.timer.purge();
        instance = null;
    }

    public static XhiftTracker getInstance() {
        return instance;
    }

    public static boolean hasInstance() {
        return (instance != null);
    }

    private BencodeList getPeers(BencodeString info_hash, BencodeString peer_id) {
        PeerList peers = allPeers.get(info_hash);
        BencodeList response = new BencodeList();

        if (peers == null) {
            return response;
        }

        for (BencodeString key : peers.keySet()) {
            if (peers.get(key).get("peer id").equals(peer_id)) {
                continue;
            }
            response.add(peers.get(key));
        }

        return response;
    }

    public void insertPeer(BencodeString info_hash, BencodeString peer_id, BencodeString ip, int port) {

        BencodeDictionary newPeer = new BencodeDictionary();
        newPeer.put("peer id", peer_id);
        newPeer.put("port", port);
        newPeer.put("ip", ip);

        PeerList peers;

        if (this.allPeers.keySet().contains(info_hash)) {
            peers = this.allPeers.get(info_hash);
        } else {
            peers = new PeerList();
            this.allPeers.put(info_hash, peers);
            System.out.println("Inserted: " + newPeer.getJSON());
        }

        peers.put(peer_id, newPeer);
    }

    public BencodeDataType getResponse(BencodeString info_hash, BencodeString peer_id) {
        BencodeDictionary response = new BencodeDictionary();

        BencodeList peers = getPeers(info_hash, peer_id);

        response.put("interval", ANNOUNCE_INTERVAL);
        response.put("peers", peers);

        System.out.println("Responded: " + response.getBencode());

        return response;
    }

    private class PeerList extends HashMap<BencodeString, BencodeDictionary> {

        private final HashMap<BencodeString, Date> timestamp = new HashMap<>();

        @Override
        public BencodeDictionary put(BencodeString key, BencodeDictionary item) {
            timestamp.put(key, new Date());
            return super.put(key, item);
        }

        public void refresh() {
            for (BencodeString key : this.keySet()) {
                if ((int) (new Date().getTime() - timestamp.get(key).getTime()) > TIME_TO_LIVE) {
                    timestamp.remove(key);
                    this.remove(key);
                }
            }
        }

    }
}
